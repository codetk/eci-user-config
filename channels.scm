;; Add my personal packages to those Guix provides.
(cons (channel
        (name 'eci-pkg-menu)
        (url "https://git-r3lab.uni.lu/eci/eci-pkg-menu.git"))
      %default-channels)
