;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
    (not (gnutls-available-p))))
    (proto (if no-ssl "http" "https")))
    ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
    (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
    ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
    (when (< emacs-major-version 24)
      ;; For important compatibility libraries like cl-lib
      (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))

(package-initialize)
(require 'ob-tangle)

(defun eci-tangle-emacs-org ()
 "Tangles .emacs.org and byte compiles ~/.emacs.d/"
   (interactive)
   (when (equal (buffer-name)
                (concat ".eci.emacs.org"))
     (org-babel-tangle)
     (byte-recompile-directory (expand-file-name user-emacs-directory) 0)))

(defun eci-tangle-emacs-org-file ()
 "Tangles .emacs.org and byte compiles ~/.emacs.d/"
 (org-babel-tangle-file  "~/.eci.emacs.org")
 (byte-recompile-directory (expand-file-name user-emacs-directory) 0))

(add-hook 'after-save-hook #'eci-tangle-emacs-org)
(add-hook 'kill-emacs-hook #'eci-tangle-emacs-org)
;; (add-hook 'org-export-before-processing-hook #'org-update-all-dblocks)




(defvar eci-emacs-org-el  "~/.emacs.d/doteciemacs.org.el")
(if (file-exists-p eci-emacs-org-el) (load eci-emacs-org-el) nil)
(package-initialize)
