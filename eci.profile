# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

GUIX_PROFILE="$HOME/.guix-profile"
if [ -d $GUIX_PROFILE ] || [ -h $GUIX_PROFILE ];then
    export GUIX_PROFILE
    export GUIX_LOCPATH=$GUIX_PROFILE/lib/locale/
    export SSL_CERT_DIR="$HOME/.guix-profile/etc/ssl/certs"
    export SSL_CERT_FILE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"
    export GIT_SSL_CAINFO="$SSL_CERT_FILE"
    export SSL_CERT_FILE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"
    export CURL_CA_BUNDLE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"
    export BASH_LOADABLE_PATH="$HOME/.guix-profile/lib/bash${BASH_LOADABLES_PATH:+:}$BASH_LOADABLES_PATH"
    export GIT_EXEC_PATH="$HOME/.guix-profile/libexec/git-core"
    source $GUIX_PROFILE/etc/profile
    export PATH="$HOME/.config/guix/current/bin${PATH:+:}$PATH"
    hash guix
fi