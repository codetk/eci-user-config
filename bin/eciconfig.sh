#!/home/eci/.guix-profile/bin/bash




## Data
LREPO=$HOME/.config/eci-user-config

## Files
declare -A DESTF
DESTF[eci.emacs.org]="$HOME/.eci.emacs.org"
DESTF[eci.profile]="$HOME/.eci.profile"
DESTF[Rprofile]="$HOME/.Rprofile"
DESTF[Xclients]="$HOME/.Xclients"
DESTF[Xdefaults]="$HOME/.Xdefaults"
DESTF[openbox]="$HOME/.config/openbox"
DESTF[eci.manifest.scm]="$HOME/.config/eci.manifest.scm"

declare -A DESTD
DESTD[openbox]="$HOME/.config/openbox"

## Aux functions
message () {
    echo "$@" >& 2
}

title () {
    message === "$@" ===
}

title1 () {
    message ">==" "$@" ==
}

title2() {
    message ">>=" "$@" =
}

exists() {
    hash "$1" 2>/dev/null || exit 1
}

fake () {
    echo "$@"
}




## Preparation
mkdir -p "$HOME/bin"

ecipack () {
    message
    title1 Installing Manifest Packages
    guix pull
    guix package -c4 -m ${DESTF['eci.manifest.scm']}
    title1 Finished Installing Manifest Packages
}

eciupdaterepo () {
    message
    title1 Updating \"$L{REPO}\" Repository
    (
    cd $LREPO
    git pull
    )
    title1 Finished updating \"${LREPO}\"
}
emacsconf () {
    message
    title1 Configuring emacs
    if [[ ! -e "$HOME/.emacs" ]]; then
	cp "$LREPO/eci.emacs" "$HOME/.emacs"
    else
	message Not touching user\'s already existing .emacs.
    fi
    title2 Set up automatic customisation file.
    if [[ ! -e "$HOME/.emacs.d/custom.el" ]]; then
	touch "$HOME/.emacs.d/custom.el"
    fi
    title2 Finish setting up automatic customisation file.
    message
    title2 Refreshing Config
    emacs --batch -l "$HOME/.emacs" -f eci-custom
    emacs --batch -l "$HOME/.emacs" -f eci-tangle-emacs-org-file
    title2 Finished Refreshing Config
    title1 Finished Configuring Emacs

}

eciprofile () {
    PROFILE=$HOME/.bash_profile
    message
    title1 Configuring profile
    if [[ ! $(grep -i "source ${DESTF[eci.profile]}" "$PROFILE") ]]; then
	title2 Adding eci profile
	echo "source ${DESTF[eci.profile]}" >> "$PROFILE"
    fi
    if [[ ! $(grep -i "source $GUIX_PROFILE/etc/profile" "$PROFILE") ]]; then
	title2 Adding guix profile
	echo  "source $GUIX_PROFILE/etc/profile" >> "$PROFILE"
    fi
}

eciopenbox () {
    message
    title1 Configuring openbox
    openbox --reconfigure
    title1 Finished configuring openbox
}

eciinstall () {
    message === FILE INSTALLATION STAGE ===
    for fn in ${!DESTF[@]}; do
	message Copying "$fn"
	rm -f "${DESTF[$fn]}"
	cp -v "$LREPO/$fn" "${DESTF[$fn]}"
    done

    for dr in ${!DESTD[@]}; do
	message Copying "$dr"
	rm -Rf "${DESTD[$dr]}"
	cp -vR "$LREPO/$dr" "${DESTD[$dr]}"
    done

    ecipack

    message
    message === FINISHED FILE INSTALLATION STAGE ===



}

eciconfig () {
    title CONFIGURATION STAGE
    eciprofile
    emacsconf
    eciopenbox
    title FINISHED CONFIGURATION STAGE
}


usage () {
    message [--install [--update [--help [--config]]]] [-icuh]
}


eciprolog() {
    ARGS=$(getopt -l install,config,update,help -o ":ciuh" -- "$@")
    [ "$?" -eq 0 ] || {
	usage
	exit -1
    }

    set -- $ARGS
    while :; do
	
	case $1 in
	    "--" ) break
		   ;;
	    "--install" | "-i")
		eciinstall
		;;
	    "--config" | "-c")
		eciconfig
		;;
	    "--update" | "-u")
		eciupdaterepo
		;;
	    "--help" | "-h")
		usage
		;;
	    *)
		message "Error, unable to interpret argument: `$1` ."
		usage
		exit -1
	       ;;

	esac

	shift

    done


}

eciprolog "$@"
#eciinstall
#eciconfig

